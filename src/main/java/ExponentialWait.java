public class ExponentialWait {

    private final int baseWaitTimeInMs;

    public ExponentialWait(int baseWaitTimeInMs) {
        this.baseWaitTimeInMs = baseWaitTimeInMs;
    }

    public void waitForNextAttempt(int attemptNumber) {
        try {
            Thread.sleep(calculateDelayMultiplier(attemptNumber) * baseWaitTimeInMs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private int calculateDelayMultiplier(int attemptNumber) {
        return (int) ((1 << attemptNumber) * Math.random());
    }

}
