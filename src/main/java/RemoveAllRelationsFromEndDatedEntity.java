import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class RemoveAllRelationsFromEndDatedEntity {


    static Properties prop = new Properties();

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Input Properties or Mode  not specified");
            return;
        }
        String mode = args[0];
        String file = args[1];

        System.out.println("mode=" + mode);
        System.out.println("file=" + file);

        RemoveAllRelationsFromEndDatedEntity removeAllRelationsFromEndDatedEntity = new RemoveAllRelationsFromEndDatedEntity();

        if (mode.equalsIgnoreCase("read")) {
            removeAllRelationsFromEndDatedEntity.getEntities(file);
        } else if (mode.equalsIgnoreCase("fix")) {
            removeAllRelationsFromEndDatedEntity.fixEntities(file);
        } else {
            System.out.println("Mode is not correct , should be read  or fix ");
            return;
        }

    }

    @SuppressWarnings("Duplicates")
    private void getEntities(String file) throws Exception {
        prop.load(new FileReader(file));
        System.out.format("USER=%s \n" +
                        " PASSWORD=%s \n" +
                        " AUTH_URL=%s \n" +
                        " TENANT=%s \n" +
                        " SERVER_URI=%s \n",
                prop.getProperty("USER"),
                prop.getProperty("PASSWORD"),
                prop.getProperty("AUTH_URL"),
                prop.getProperty("TENANT"),
                prop.getProperty("SERVER_URI")
        );
        AuthClient authClient = new AuthClient(
                prop.getProperty("AUTH_URL"),
                prop.getProperty("USER"),
                prop.getProperty("PASSWORD"));

        RestApiClient restApiClient = new RestApiClient(
                prop.getProperty("SERVER_URI"),
                prop.getProperty("TENANT"),
                authClient
        );

        List<String> entityList = restApiClient.getEntitiesFromReltio();
        System.out.println("### Inactive Entities  ,  to be Fixed ### ===>  " + entityList);
    }

    @SuppressWarnings("Duplicates")
    private void fixEntities(String file) throws Exception {
        prop.load(new FileReader(file));
        System.out.format("USER=%s \n" +
                        " PASSWORD=%s \n" +
                        " AUTH_URL=%s \n" +
                        " TENANT=%s \n" +
                        " SERVER_URI=%s \n",
                prop.getProperty("USER"),
                prop.getProperty("PASSWORD"),
                prop.getProperty("AUTH_URL"),
                prop.getProperty("TENANT"),
                prop.getProperty("SERVER_URI")
        );
        AuthClient authClient = new AuthClient(
                prop.getProperty("AUTH_URL"),
                prop.getProperty("USER"),
                prop.getProperty("PASSWORD"));

        RestApiClient restApiClient = new RestApiClient(
                prop.getProperty("SERVER_URI"),
                prop.getProperty("TENANT"),
                authClient
        );

        List<String> inactiveEntities = restApiClient.getEntitiesFromFile();
        System.out.println("### Inactive Entities from file ###  ===>  " + inactiveEntities);
        System.out.println("count : " + inactiveEntities.size());


        for (String entityId : inactiveEntities) {
            System.out.println("##########---Processing : " + entityId + " ------#########");
            JSONObject entityObject = restApiClient.getEntity(entityId);
            JSONArray CWArray = (JSONArray) entityObject.get("crosswalks");
            String deleteDate = ((JSONObject) CWArray.get(0)).get("deleteDate").toString();
            JSONArray relationships = restApiClient.getConnectionsFromEntityId(entityId);
            for (int i = 0; i < relationships.length(); i++) {
                JSONObject relationshipData = (JSONObject) relationships.get(i);
                if (Integer.parseInt(relationshipData.get("total").toString()) > 0) {
                    JSONArray connections = relationshipData.getJSONArray("connections");
                    for (int j = 0; j < connections.length(); j++) {
                        JSONObject connection = (JSONObject) connections.get(j);
                        JSONObject relation = (JSONObject) connection.get("relation");
                        String relationUri = relation.get("relationUri").toString();
                        restApiClient.updateRelationEndDate(relationUri, deleteDate);
                    }
                }
            }

            System.out.println("##########--- Completed : " + entityId + " ------#########");

        }
    }
}
