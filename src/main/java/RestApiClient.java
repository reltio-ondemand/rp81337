import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


public class RestApiClient {

    private String server_uri;
    private String tenant;
    private AuthClient authClient;
    private String accessToken = null;

    public RestApiClient(String server_uri, String tenant, AuthClient authClient) {
        this.server_uri = server_uri;
        this.tenant = tenant;
        this.authClient = authClient;
    }


    public List<String> getEntitiesFromReltio() throws Exception {
        List<String> entityList = new ArrayList<>();
        int totalCount = getEntitiesCount();
        int processedCount = 0;
        BufferedWriter out = new BufferedWriter(
                new FileWriter(String.format("entitiesToBeFixed_%s.txt" , tenant), false));
        try {
            while (processedCount <= totalCount) {
                String url = String.format("%s/api/%s/entities?activeness=not_active&offset=%s&select=uri",
                        server_uri,
                        tenant,
                        processedCount
                );
                String responseContent = getObjectFromUrl(url);
                JSONArray jsonArray = new JSONArray(responseContent);
                for (int i = 0; i < jsonArray.length(); i++) {
                    String entity = ((JSONObject) jsonArray.get(i)).get("uri").toString();
                    System.out.println("checking Inactive profile : " + entity);
                    if(isValidForRemediation(entity)) {
                        out.write(entity + System.lineSeparator());
                        entityList.add(entity);
                    }
                }
                processedCount = processedCount + 200;
            }
        } finally {
            out.close();
        }
        return entityList;
    }

    public Boolean isValidForRemediation(String entityId) throws Exception
    {
        JSONArray relationships = getConnectionsFromEntityId(entityId);
        for (int i = 0; i < relationships.length(); i++) {
            JSONObject relationshipData = (JSONObject) relationships.get(i);
            if (Integer.parseInt(relationshipData.get("total").toString()) > 0) {
               return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private int getEntitiesCount() throws Exception {
        String url = String.format("%s/api/%s/entities/_total?activeness=not_active&select=uri",
                server_uri,
                tenant
        );
        String responseContent = getObjectFromUrl(url);
        JSONObject jsonObject = new JSONObject(responseContent);
        String total = jsonObject.get("total").toString();
        return Integer.parseInt(total);
    }

    public List<String> getEntitiesFromFile() throws Exception {
        String str = "";
        List<String> entitiesFromFile = new ArrayList<String>();
        InputStream is = new FileInputStream(String.format("entitiesToBeFixed_%s.txt" , tenant));
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        if (is != null) {
            while ((str = reader.readLine()) != null) {
                entitiesFromFile.add(str);
            }
        }
        return entitiesFromFile;
    }

    public String getPathSpecification() throws Exception {
        String str = "";
        InputStream is = new FileInputStream("pathSpecification.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        if (is != null) {
            str = reader.readLine();

        }
        return str;
    }


    public JSONObject getEntity(String entityId) throws Exception {
        System.out.println("entityId=" + entityId);
        String url = String.format("%s/api/%s/%s",
                server_uri,
                tenant,
                entityId);
        String responseContent = getObjectFromUrl(url);
        return new JSONObject(responseContent);
    }


    public JSONArray getConnectionsFromEntityId(String entityId) throws Exception {
        if (accessToken == null) {
            accessToken = authClient.getAccessToken();
        }
        HttpPost getConnection = new HttpPost(String.format("%s/api/%s/%s/_connections?limitCreditsConsumption=true&activeness_enabled=true",
                server_uri,
                tenant,
                entityId));

        getConnection.setHeader("Authorization", String.format("Bearer %s", accessToken));
        getConnection.setHeader("Content-Type", "application/json");

        getConnection.setEntity(new StringEntity(getPathSpecification()));

        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(getConnection);
        if (response.getStatusLine().getStatusCode() == 401) {
            System.out.println("Auth Token is Invalid trying again..!");
            accessToken = null;
            getConnectionsFromEntityId(entityId);
        }
        else if (response.getStatusLine().getStatusCode() >= 400) {
            throw new Exception(String.format("Entity %s getting error: %s", entityId, response.getStatusLine().getStatusCode()));
        }
        String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
        return new JSONArray(responseContent);
    }


    public String updateRelationEndDate(String relationId, String endDate) throws Exception {
        if (accessToken == null) {
            accessToken = authClient.getAccessToken();
        }
        HttpPut updateEndDate = new HttpPut(String.format("%s/api/%s/%s/activeness",
                server_uri,
                tenant,
                relationId));

        updateEndDate.setHeader("Authorization", String.format("Bearer %s", accessToken));
        updateEndDate.setHeader("Content-Type", "application/json");

        long timeInMillis = getTimeInMillis(endDate);

        updateEndDate.setEntity(new StringEntity(String.format("{\"endDate\": %s}", timeInMillis)));

        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(updateEndDate);
        if (response.getStatusLine().getStatusCode() == 401) {
            System.out.println("Auth Token is Invalid trying again..!");
            accessToken = null;
            updateRelationEndDate(relationId , endDate);
        }
        else if (response.getStatusLine().getStatusCode() >= 400) {
            throw new Exception(String.format("Error while UPdating Relations"));
        }
        String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
        return responseContent;

    }

    public long getTimeInMillis(String date) {
        String input = date;
        DateTimeZone zone = DateTimeZone.forID(DateTimeZone.UTC.toString());
        DateTime dateTime = new DateTime(input, zone);
        long timeInMillis = dateTime.getMillis();
        return timeInMillis;
    }

    public String getObjectFromUrl(String url) throws Exception {
        if (accessToken == null) {
            accessToken = authClient.getAccessToken();
        }
        HttpGet getEntity = new HttpGet(url);
        getEntity.setHeader("Authorization", String.format("Bearer %s", accessToken));
        getEntity.setHeader("Content-Type", "application/json");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(getEntity);
        if (response.getStatusLine().getStatusCode() == 401) {
            System.out.println("Auth Token is Invalid trying again..!");
            accessToken = null;
            getObjectFromUrl(url);
        }
        if (response.getStatusLine().getStatusCode() >= 400) {
            throw new Exception("Error while getting entities");
        }
        String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
        return responseContent;
    }
}
