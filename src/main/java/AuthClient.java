import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


import java.io.IOException;

public class AuthClient {

    private static final int maxAuthAttemptCount = 64;

    private final String user;
    private final String password;
    private final String authServer;
    private ObjectMapper objectMapper = new ObjectMapper();
    private ExponentialWait exponentialWait = new ExponentialWait(100);
    private String accessToken;
    private String refreshToken;
    private long needUpdateTokenAfter;


    public AuthClient(String authServer, String user, String password) {
        this.authServer = authServer;
        this.user = user;
        this.password = password;
    }

    private static String getAuthRequestContent(String user, String password) {
        return "username={{username}}&password={{password}}&grant_type=password"
                .replace("{{username}}", user)
                .replace("{{password}}", password);
    }

    public synchronized String getAccessToken() {
        long currentTimeStamp = System.currentTimeMillis();
        if (needUpdateTokenAfter <= currentTimeStamp || null == accessToken) {
            try {
                AuthResponse authResponse = getAuthInfo();
                if (authResponse == null) {
                    return null;
                }
                accessToken = authResponse.getAccessToken();
                refreshToken = authResponse.getRefreshToken();
                needUpdateTokenAfter = System.currentTimeMillis() + authResponse.getExpiresIn();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return accessToken;
    }

    private AuthResponse getAuthInfo() throws IOException {
        String result;
        HttpPost post = new HttpPost(authServer);
        post.setHeader("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setEntity(new StringEntity(getAuthRequestContent(user, password)));

        int attempt = 0;
        while (true) {
            if (attempt == maxAuthAttemptCount) {
                System.out.println("maxAuthAttemptCount reached, exiting");
                return null;
            }
            exponentialWait.waitForNextAttempt(attempt);
            try (CloseableHttpClient httpClient = HttpClients.createDefault();
                 CloseableHttpResponse response = httpClient.execute(post)) {
                result = EntityUtils.toString(response.getEntity());
                break;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            attempt++;
        }
        return result != null ? objectMapper.readValue(result, AuthResponse.class) : null;
    }

    static class AuthResponse {

        private String accessToken;
        private String tokenType;
        private String refreshToken;
        private int expiresIn;
        private String scope;

        public AuthResponse() {
        }

        public String getAccessToken() {
            return accessToken;
        }

        @JsonSetter("access_token")
        public AuthResponse setAccessToken(String accessToken) {
            this.accessToken = accessToken;
            return this;
        }

        public String getTokenType() {
            return tokenType;
        }

        @JsonSetter("token_type")
        public AuthResponse setTokenType(String tokenType) {
            this.tokenType = tokenType;
            return this;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        @JsonSetter("refresh_token")
        public AuthResponse setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
            return this;
        }

        public int getExpiresIn() {
            return expiresIn;
        }

        @JsonSetter("expires_in")
        public AuthResponse setExpiresIn(int expiresIn) {
            this.expiresIn = expiresIn;
            return this;
        }

        public String getScope() {
            return scope;
        }

        @JsonSetter("scope")
        public AuthResponse setScope(String scope) {
            this.scope = scope;
            return this;
        }

        @Override
        public String toString() {
            return "AuthResponse{" +
                    "accessToken='" + accessToken + '\'' +
                    ", tokenType='" + tokenType + '\'' +
                    ", refreshToken='" + refreshToken + '\'' +
                    ", expiresIn='" + expiresIn + '\'' +
                    ", scope='" + scope + '\'' +
                    '}';
        }
    }

}
