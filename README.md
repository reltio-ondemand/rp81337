

### How to Run? ###

1. copy RP81337-1.0-SNAPSHOT-jar-with-dependencies.jar and pathSpecification.txt in same directory

2. copy sample.properties  in any directory and update the details as needed .

3. run command : 
                
			java -jar <jar_name> <mode> <sample.properties file path>

4. run in read mode : java -jar <jar_name> read <sample.properties file path>

      Example => java -jar RP81337-1.0-SNAPSHOT-jar-with-dependencies.jar read C:\Users\Hp\Desktop\sample.properties 
	
              --> it will create entitiesToBeFixed_<tenant_id>.txt  in same directory , which will have all entities to be fixed 
   
3. run in fix mode : java -jar <jar_name> fix <sample.properties file path>

      Example => java -jar RP81337-1.0-SNAPSHOT-jar-with-dependencies.jar fix C:\Users\Hp\Desktop\sample.properties 

              --> it will take all entities from entitiesToBeFixed_<tenant_id>.txt and fix them . 
			  
			  
			  
### Recommendations
    
	            1. Before running in fix mode , you can  have a look  on entities from entitiesToBeFixed_<tenant_id>.txt . these are the entities which will be affected when run in fix mode . 
                 
                2. It is recommended to run script in read mode always before fix mode . 

                3. In case we want to fix few selected entities , we can manually update entitiesToBeFixed_<tenant_id>.txt	file and run in fix mode .		
